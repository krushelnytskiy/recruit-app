# Auth Microservice API v0.1.0

Auth Microservice API

- [Login](#login)
	- [Authorize user](#authorize-user)
	
- [Register](#register)
	- [Register client](#register-client)
	- [Register user](#register-user)
	


# Login

## Authorize user
[Back to top](#top)



	POST /api/login/authorize





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
| client_id | String | <p>Client ID</p>|
| client_secret | String | <p>Client Secret</p>|
| redirect_url | String | <p>Redirect URL</p>|


### Success 200

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
| Grant | JSON | <p>JSON Object</p>|

# Register

## Register client
[Back to top](#top)



	POST /api/login/authorize





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
| client_id | String | <p>Client ID</p>|
| client_secret | String | <p>Client Secret</p>|
| redirect_url | String | <p>Redirect URL</p>|


### Success 200

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
| Grant | JSON | <p>JSON Object</p>|

## Register user
[Back to top](#top)



	POST /api/login/authorize





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
| client_id | String | <p>Client ID</p>|
| client_secret | String | <p>Client Secret</p>|
| redirect_url | String | <p>Redirect URL</p>|


### Success 200

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
| Grant | JSON | <p>JSON Object</p>|

