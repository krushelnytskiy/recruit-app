let express = require('express');
let modules = require('./modules/');

let app = express();

// Helpers
modules(app);

module.exports = app;