let stringHash = require('string-hash');

/**
 * @returns {string}
 */
module.exports = () => {
    return stringHash(new Date('h')) + Math.random().toString(36).substr(2, 9);
};