module.exports = {
    generateSecret: require('./generate-secret'),
    generateToken: require('./generate-token'),
};