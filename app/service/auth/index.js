const request = require('request');
const qs = require('querystring');

const credentials = require('./config/credentails.json');

const links = {
    loginLink: `${credentials.base}/login/${credentials.client_id}`,
    registerLink: `${credentials.base}/register/${credentials.client_id}`
};

let result = {
    links
};

result.getUserDetails = function (token, onSuccess, onError) {
    const query = qs.stringify(
        {
            clientId: credentials.client_id,
            clientSecret: credentials.client_secret,
        }
    );

    request.get(
        {
            url: `${credentials.base}/api/login/users/${token}?${query}`
        },
        (error, response, body) => {

            if (error) {
                onError(error);
            } else {
                onSuccess(JSON.parse(body));
            }

        }
    )
};

result.authorize = function (app, next) {

    request.post(
        {
            url: credentials.base + '/login/authorize',
            form: {
                clientId: credentials.client_id,
                clientSecret: credentials.client_secret,
                redirectUri: 'http://localhost:4000',
                grantLevel: 'FULL'
            }
        },
        (error, response, body) => {
            app.locals.loginLink = JSON.parse(body).link;
            next();
        }
    );

};

module.exports = result;