module.exports = function(sequelize, Sequelize) {

    return sequelize.define(
        "Vacation",
        {
            title: {
                type: Sequelize.STRING,
                field: 'title'
            },
            user: {
                type: Sequelize.STRING,
                field: 'user'
            },
            description: {
                type: Sequelize.STRING,
                field: 'description'
            },
            createdAt: {
                type: Sequelize.DATE
            },
            updatedAt: {
                type: Sequelize.DATE
            }
        },
        {
            classMethods: {
                tableName: 'vacations'
            }
        }
    );
};