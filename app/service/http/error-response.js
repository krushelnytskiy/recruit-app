/**
 * @param {Error} error
 * @returns {{success: boolean, message}}
 */
module.exports = function (error) {
    return {
        success: false,
        message: error.message
    };
};