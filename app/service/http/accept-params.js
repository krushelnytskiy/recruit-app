/**
 * @param {Object} params
 * @param {Array} requiredParams
 * @param {Object} optionalParams
 * @throws Error
 */
module.exports = function(params, requiredParams, optionalParams) {

    // return object
    let result = {};

    // Required params loop
    requiredParams.forEach(function (requiredParam) {
        if (undefined === params[requiredParam]) {
            throw new Error('Param ' + requiredParam + ' cannot be empty');
        } else {
            result[requiredParam] = params[requiredParam];
        }
    });

    // Optional params loop
    for (let optionalParam in optionalParams) {
        result[optionalParam] = undefined === params[optionalParam]
            ? optionalParams[optionalParam]
            : params[optionalParam];
    }

    return result;
};