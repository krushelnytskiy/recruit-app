let express = require('express'),
    router = express.Router();

router.get('/new-vacation', require('../midleware/authorized'), require('./create'));
router.get('/edit-vacation/:id', require('../midleware/authorized'), require('./edit'));

router.post('/save-vacation', (req, res) => {
    let db = req.app.appRequire('service', 'db', 'db');
    db.Vacation.create(
        {
            title: req.body.title,
            description: req.body.description,
            user: req.app.locals.user.id
        }
    ).then(
        vacation => {
            res.redirect('/');
        }
    ).catch(
        error => {
            res.render('error');
        }
    );

});

router.post('/edit-vacation/:id', (req, res) => {
    let db = req.app.appRequire('service', 'db', 'db');
    db.Vacation.update(
        {
            title: req.body.title,
            description: req.body.description,
        },
        {
            where: {
                id: req.params.id
            }
        }
    ).then(
        vacation => {
            res.redirect('/');
        }
    ).catch(
        error => {
            res.render('error');
        }
    );
});

module.exports = router;