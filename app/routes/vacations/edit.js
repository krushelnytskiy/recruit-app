module.exports = (req, res, next) => {
    let db = req.app.appRequire('service', 'db', 'db');
    db.Vacation.findByPrimary(req.params.id).then(vacation => {
        if (null === vacation) {
            next();
        } else {
            res.render('vacations/edit', {
                vacation: vacation,
                action: '/edit-vacation/' + vacation.id,
                inputs: {
                    vacation: vacation.id
                }
            });
        }
    });
};