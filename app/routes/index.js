let path = require('path'),
    express = require('express'),
    router  = express.Router();

const auth = require('../service/auth');

router.use(
    (req, res, next) => {
        req.app.locals.loggedIn = typeof req.session.token === 'string';
        req.app.locals.auth = auth;
        if (req.app.locals.loggedIn && !req.app.locals.user) {
             auth.getUserDetails(
                 req.session.token,
                 user => {
                     req.app.locals.user = user;
                     next();
                 },
                 error => {
                     res.render('error');
                 }
            );
        } else if (!req.app.locals.loggedIn && req.app.locals.user) {
            req.app.locals.user = undefined;
            next();
        } else {
            next();
        }
    }
);

router.get('/login', (req, res) => {
    if (req.query.token) {
        req.session.token = req.query.token;
        req.session.save();
    }

    res.redirect('/');
});

router.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/');
});

router.get('/', (req, res) => {

    let db = req.app.appRequire('service', 'db', 'db');
    db.Vacation.findAll().then(
        vacations => {
            res.render('index', {
                title: 'Recruit APP',
                vacations: vacations
            });
        },
        () => {
            res.render('index', {
                title: 'Recruit APP',
                vacations: []
            });
        }
    );

});

router.use(require('./vacations'));

module.exports = router;