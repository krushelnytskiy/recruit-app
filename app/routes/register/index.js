let express = require('express'),
    router = express.Router();

router.get('/register', require('./get'));
router.post('/register', require('./post'));

module.exports = router;