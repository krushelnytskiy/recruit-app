let path = require('path');

/**
 * Require file relatively to app path
 * @returns {*}
 */
let appRequire = function () {
    return require(path.join(__dirname, '..', '..', path.join.apply(null, arguments)));
};

module.exports = function (app) {
    app.appRequire = appRequire;
};