let redis   = require("redis");
let session = require('express-session');

module.exports = app => {
    app.use(session({
        secret: 'recruit-app',
        saveUninitialized: false,
        resave: false
    }));
};