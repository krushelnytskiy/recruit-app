let helpers = app => {

    let errors       = require('./errors'),
        bodyParser   = require('./body-parser'),
        cookie       = require('./cookie'),
        logger       = require('./logger'),
        staticModule = require('./static'),
        views        = require('./views'),
        functions    = require('./functions/'),
        session      = require('./session'),
        router       = require('./router'),
        environment  = require('./environment');

    environment(app);
    bodyParser(app);
    cookie(app);
    logger(app);
    functions(app);
    session(app);
    views(app);
    staticModule(app);
    router(app);
    errors(app);

};

module.exports = helpers;