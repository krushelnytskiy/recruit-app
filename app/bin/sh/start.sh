#!/usr/bin/env bash
node_modules/.bin/sequelize \
    --config service/db/config/connection.json \
    --options-path service/db/config/options.json \
    db:migrate

npm install
npm start