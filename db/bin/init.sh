#!/usr/bin/env bash

pg_createcluster 9.6 main --start
/etc/init.d/postgresql start

runuser -l postgres -c 'psql <<-EOSQL
    CREATE USER root;
    CREATE DATABASE "recruit-app";
    GRANT ALL PRIVILEGES ON DATABASE "recruit-app" TO root;
EOSQL'